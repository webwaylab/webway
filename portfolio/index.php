<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Проекты");
?>
<?$APPLICATION->IncludeComponent(
	"sws:super.component",
	"portfolio.list",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"IBLOCK_ID"=>3,
		"IBLOCK_NAPRAV_ID"=>4,
		"IBLOCK_OTRASLI_ID"=>5,
		"IBLOCK_YEARS_ID"=>5,
		"CODE"=>"portfolio.list"
	)
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>