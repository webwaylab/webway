$(document).ready(function(){
	$('body').attr('data-item', 1);
	$('body[data-item="1"]').addClass('item1');

	var galleryThumbs = new Swiper('.gallery-thumbs', {	loop: true });

	var galleryTop = new Swiper('.swiper-container.gallery-top', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		parallax: true,
		speed: 900,
		loop:true
	});	

	galleryTop.on('onSlideChangeStart', function (swiper) {
	  	var sindex = $('.gallery-top .swiper-slide-active').data("swiper-slide-index");
	  	var pindex = $('.gallery-top .swiper-slide-prev').data("swiper-slide-index");
	  	var nindex = $('.gallery-top .swiper-slide-next').data("swiper-slide-index");
	 	$('body').attr('data-item', sindex + 1);
	 	$('body').removeClass('item'+(nindex+1));
	 	$('body').removeClass('item'+(pindex+1));
	 	$('body').addClass('item'+(sindex+1));		
	});

	galleryTop.params.control = galleryThumbs;
	galleryThumbs.params.control = galleryTop;
});	