
$(document).ready(function(){
	 //    var swiper = new Swiper('.swiper-container', {
  //       pagination: '.swiper-pagination',
  //       paginationClickable: true,
  //       nextButton: '.swiper-button-next',
  //       prevButton: '.swiper-button-prev',
  //       parallax: true,
  //       speed: 600,
		// loop:true,
  //   });
		
	
/* Main menu opener */
	(function(){
		var animationDuration = 400;
		var headerWrap = $('.header-wrap');
		var menuOpener = $('#menu-opener');
		var menuWrapper = $('#header_menu');
		var menu = menuWrapper.find('.header_menu_wrapper');
		var menuItems = menu.find('a');

		function openMenu(){
			menuWrapper.fadeIn(animationDuration);
			headerWrap.addClass('active-menu');
			menu.animate({ 
				'left': '0' 
			}, { 
				duration: animationDuration, 
				complete: function(){
                    menuOpener.on('click', openerHandler);
				}
			});
		};
		
		function closeMenu(){
			menuWrapper.fadeOut(animationDuration);
			headerWrap.removeClass('active-menu');
			menu.animate({ 'left': '-28%' }, { 
				duration:animationDuration, 
				complete: function(){
					
					menuOpener.on('click', openerHandler);
				}
			});
		}
		
		function openerHandler(e){
			e.preventDefault();
			e.stopPropagation();
			
			menuOpener.off('click');
			
			if(headerWrap.hasClass('active-menu')){
				closeMenu();
			} else {
				openMenu();
			}
		}
		
		menuOpener.on('click', openerHandler);
		
		menuWrapper.on('click', function(e){
			e.stopPropagation();

			menuOpener.off('click');
			
			if(event.target === this){
				closeMenu();
			} 
		});
		
		menuItems.on('click', function(){
			menuOpener.off('click');
			closeMenu();
		});
	})();

/* END Main menu opener */

/*Portfolio filter*/
	
	// multiselect 
		// $('.multi').multiselect({});  
		
	//taglist


		$('.tag-list').on('click', '.tag-list_li', function() {

			$(this).remove();
			
			if (!($('.tag-list_li').length)) {
				$('.clean-filter').addClass('invisible');
				$('.jq-checkbox').removeClass('checked');
			}
			return false;
		});
		
	/*END portfolio filter*/
});
