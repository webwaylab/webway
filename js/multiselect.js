$(document).ready(function(){
	$('.multi').multiselect({
		noneSelectedText: 'По направлениям'
	}); 
	$('.multi_otr').multiselect({
		noneSelectedText: 'По отраслям'
	}); 
	$('.multi_date').multiselect({
		noneSelectedText: 'По годам'
	}); 
	$('#filter').on('change',function(){
		$(this).submit();
	});
});
