$(document).ready(function(){
	if($(window).width() > 768){
		$('.flip_list.portfolio_list .row').viewportChecker({
		  classToAdd: 'visible animated flipX',
		  offset: 0,
		  invertBottomOffset: true,
		  repeat: false,
		  callbackFunction: function(elem, action){},
		  scrollHorizontal: false
		});
		$('.flip_list.portfolio .row').viewportChecker({
		  classToAdd: 'visible animated flipX',
		  offset: -100,
		  invertBottomOffset: true,
		  repeat: false,
		  callbackFunction: function(elem, action){},
		  scrollHorizontal: false
		});
	};

});
