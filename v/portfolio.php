<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/header.php");?>

<style>body {background: #818181;}</style>

<script src="js/viewportchecker.js"></script> 
<script src="js/flip_list.js"></script> 

<section class="main top_row"> 
	<ul class="breadcrambs">
		<li><a href="#">webway</a></li>/
		<li><a href="#">проекты</a></li>/ 
		<li>теремок</li>
	</ul> 	
	
	<div class="white_container clearfix">
		<div class="portfolio_nav">
			<a href="#" class="active">Лучшие проекты</a>
			<a href="#">Проекты по направлениям</a>
		</div>

		<div class="interest portfolio flip_list clearfix"> 
			<div class="clearfix">
				<div class="item" style="background-image:url('images/tmp/best-case-1.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<div class="bottom">
						<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо» </p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
							<li>Интеграция 1С</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
				<div class="item" style="background-image:url('images/tmp/best-case-2.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">ТЕРЕМОК</h3>
					<div class="bottom">
						<p>Сайт сети, предлагающей самый традиционный из всех видов русской еды. Так же, представленна мобильная версия сайта, позволяющая делать заказы в ближайшем ресторане Теремок</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
			</div>
			<div class="clearfix">
				<div class="item" style="background-image:url('images/tmp/best-case-3.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">магазин <br>органик-маркет</h3>
					<div class="bottom">
						<p>Органик Маркет — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
				<div class="item" style="background-image:url('images/tmp/best-case-4.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">алмаз-антей</h3>
					<div class="bottom">
						<p>Органик Маркет — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
			</div>
			<div class="clearfix">
				<div class="item" style="background-image:url('images/tmp/best-case-5.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">магазин<br />органик-маркет</h3>
					<div class="bottom">
						<p>Органик Маркет — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>	
				<div class="item" style="background-image:url('images/tmp/best-case-1.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">алмаз-антей</h3>
					<div class="bottom">
						<p>Органик Маркет — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
			</div>	
			<div class="row clearfix">
				<div class="item" style="background-image:url('images/tmp/best-case-2.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<div class="bottom">
						<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо» </p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
							<li>Интеграция 1С</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
				<div class="item" style="background-image:url('images/tmp/best-case-3.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">ТЕРЕМОК</h3>
					<div class="bottom">
						<p>Сайт сети, предлагающей самый традиционный из всех видов русской еды. Так же, представленна мобильная версия сайта, позволяющая делать заказы в ближайшем ресторане Теремок</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="item" style="background-image:url('images/tmp/best-case-4.jpg');">
					<a href="#" class="link"></a>
					<h3 class="title">магазин <br>органик-маркет</h3>
					<div class="bottom">
						<p>Органик Маркет — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне</p>
						<ul>
							<li>Разработка</li>
							<li>Дизайн</li>
							<li>Поддержка</li>
						</ul>
						<p><small>Сотрудничество c 2007 по настоящее время</small></p>
					</div>
				</div>

			</div>			
		</div>	
		<div class="bottom_block"></div>
	</div>

</section>

<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/footer.php");?>   


