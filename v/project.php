<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/header.php");?>

<style>body {background: linear-gradient(90deg,#b23921,#d75f01);}</style>

<!-- Link Swiper's -->
<link rel="stylesheet" href="js/swiper/swiper.min.css">
<script src="js/swiper/swiper.min.js"></script>

<section class="main top_row"> 
	<ul class="breadcrambs">
		<li><a href="#">webway</a></li>/
		<li><a href="#">проекты</a></li>/ 
		<li>теремок</li>
	</ul> 	
	<div class="clearfix relay">
		<div class="fl_l"><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i>Керала аюрведа</a></div>
		<div class="fl_r"><a href="#">Органик-маркет<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
	</div>
	<div class="project_head white_container clearfix">
		<h1 class="title" style="color: #cb1300">Теремок</h1>
		<h3 class="second_title">Разработка и поддержка корпоративного сайта возможно что-то еще</h3>	
		<p class="description"></p>
		<span class="date">2015 — Настоящее время</span>
		<ul class="project_menu">
			<li><a href="#">Дизайн</a></li>            
			<li><a href="#">Разработка</a></li>            
			<li><a href="#">Интеграция</a></li>            
			<li><a href="#">Адаптив</a></li>
		</ul>
	</div>
</section>

<section>
	<div class="project_gallery">
		<div class="center_container"><div class="background" style="background-image: url('images/tmp/teremok/teremok_bg.jpg');"></div></div>
		<!-- Swiper -->
	    <div class="swiper-container">
	        <div class="swiper-wrapper">
	            <div class="swiper-slide">
	            	<img src="images/tmp/teremok/teremok_macbook.png" alt="">
	            </div>
	            <div class="swiper-slide">
	            	<img src="images/tmp/teremok/teremok_iphone.png" alt="">
	            </div>
	            <div class="swiper-slide">
	            	<img src="images/tmp/teremok/teremok_macbook.png" alt=""> 
	            </div>
	        </div>
	        <!-- Add Pagination -->
	        <div class="swiper-pagination"></div>
			<!-- Add Navigation -->
			<div class="swiper-button-prev swiper-button-white"></div>
			<div class="swiper-button-next swiper-button-white"></div>
	    </div>

	    <!-- Initialize Swiper -->
	    <script>
	    var swiper = new Swiper('.swiper-container', {
	        pagination: '.swiper-pagination',
	        centeredSlides: true,
	        paginationClickable: true,
	        spaceBetween: 0,

	        nextButton: '.swiper-button-next',
	        prevButton: '.swiper-button-prev',
	        slidesPerView: 'auto',
	        loop: true

	    });
	    </script>
	</div>
</section>

<section class="main bottom_row">
	<div class="project_item_list"> 
		<ul class="col-3 clearfix">
			<li>
				<span class="number">480-1920</span>
				<strong>Адаптивный сайт — любой экран</strong>
				<p>Описание преимуществ дизайна для данного проекта, должно умещаться в малый абзац и быть предельно понятным</p>
			</li>
			<li>
				<span class="top"></span>
				<span class="number">800</span>
				<strong>Позиций в меню</strong>
				<p>Для данного проекта, должно умещаться в малый абзац и быть предельно понятным</p>
			</li>
			<li>
				<span class="number">234</span>
				<strong>Пункта предоставления услуг</strong>
				<p>Описание преимуществ дизайна для данного проекта</p>
			</li>
		</ul>
		<ul class="col-2 clearfix">
			<li>
				<span class="number">980</span>
				<strong>Онлайн-посещений в час</strong>
				<p>Описание преимуществ дизайна для данного проекта, должно умещаться в малый абзац и быть предельно понятным</p>
			</li>
			<li>
				<span class="number">3500</span>
				<strong>Отывов о дизайне мобильной версии</strong>
				<p>Описание преимуществ дизайна для данного проекта, должно умещаться в малый абзац и быть предельно понятным</p>
			</li>
		</ul>
	</div>
	<div class="project_details">
		<div class="text">
			<h3 class="title">детали проекта</h3>
			<p>На сегодня под маркой «Теремок» работает 203 ресторана и 59 уличных киосков и павильонов. Cеть «Теремок» представлена в трёх форматах: уличные киоски, рестораны и кафе с собственным посадочным залом и рестораны в торговых центрах на фуд-кортах.</p>
			<p>Наряду с этим деонтология дискредитирует непредвиденный здравый смысл. Надстройка, по определению, подчеркивает непредвиденный позитивизм. Дискретность индуктивно творит смысл жизни. Акциденция, как принято считать, вырождена. Интеллект ментально заполняет здравый смысл, но ясен не всем. Акциденция методологически понимает под собой трансцендентальный дуализм. Представляется логичным, что бхутавада подрывает даосизм.</p>
		</div>
		<div class="gallery clearfix">
			<ul class="masonry">
				<li><img src="images/tmp/teremok/pic1.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic2.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic3.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic4.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic5.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic6.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic7.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic8.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic9.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic10.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic11.jpg" alt=""></li>
				<li><img src="images/tmp/teremok/pic12.jpg" alt=""></li>
			</ul>
		</div>
		<script>
		$('.masonry').masonry({
		  fitWidth: true
		});	
		</script>
	</div>

	<div class="project_links">
		<a href="#" class="site">teremok.ru</a>
		<a href="#" class="mobile">m.teremok.ru</a>
	</div>
	<div class="clearfix relay border">
		<div class="fl_l"><a href="#"><i class="fa fa-angle-left" aria-hidden="true"></i>Керала аюрведа</a></div>
		<div class="fl_r"><a href="#">Органик-маркет<i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
	</div>

	<div class="interest_header">
		<h3 class="second_title">Вам также будет интересно</h3>	
	</div>
</section> 

<section>
	<div class="interest col3 clearfix"> 
		<div class="item" style="background-image:url('images/tmp/best-case-1.jpg');">
			<a href="#" class="title">CROCUS CITY HALL</a>
			<div class="bottom">
				<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо» </p>
				<ul>
					<li>Разработка</li>
					<li>Дизайн</li>
					<li>Поддержка</li>
					<li>Интеграция 1С</li>
				</ul>
				<p><small>Сотрудничество c 2007 по настоящее время</small></p>
			</div>
		</div>
		<div class="item" style="background-image:url('images/tmp/best-case-2.jpg');">
			<a href="#" class="title">ТЕРЕМОК</a>
			<div class="bottom">
				<p>Сайт сети, предлагающей самый традиционный из всех видов русской еды. Так же, представленна мобильная версия сайта, позволяющая делать заказы в ближайшем ресторане Теремок</p>
				<ul>
					<li>Разработка</li>
					<li>Дизайн</li>
					<li>Поддержка</li>
				</ul>
				<p><small>Сотрудничество c 2007 по настоящее время</small></p>
			</div>
		</div>
		<div class="item" style="background-image:url('images/tmp/best-case-3.jpg');">
			<a href="#" class="title">магазин <br>органик-маркет</a>
			<div class="bottom">
				<p>Органик Маркет — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне</p>
				<ul>
					<li>Разработка</li>
					<li>Дизайн</li>
					<li>Поддержка</li>
				</ul>
				<p><small>Сотрудничество c 2007 по настоящее время</small></p>
			</div>
		</div>
	</div>	
</section>

<section class="main bottom_row">
	<div class="bottom_block"></div>
</section>

<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/footer.php");?>   


