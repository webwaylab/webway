<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/header.php");?>

<style>body {background: #818181;}</style>

<script src="js/multiselect.js"></script> 

<script src="js/viewportchecker.js"></script> 
<script src="js/flip_list.js"></script> 

<section class="main top_row"> 
	<ul class="breadcrambs">
		<li><a href="#">webway</a></li>/
		<li><a href="#">проекты</a></li>/ 
		<li>теремок</li>
	</ul> 	
	
	<div class="white_container clearfix">
		<div class="portfolio_nav">
			<a href="#" class="active">Лучшие проекты</a>
			<a href="#">Проекты по направлениям</a>
		</div>
	</div>
	
	<div class="clearfix">
		<form action="" class="portfolio_search icon-search-2">
			<input type="text" placeholder="Поиск..." />
			<input type="submit" class="icon-search" value="искать" />
		</form>
	</div>

	<div class="white_container clearfix">

		<form action="" class="filter_form">
			Кейсы:
			<select class="multi" multiple="multiple" name="">
				<option>FMCG (Потребительские товары)</option>							
				<option>Автомобили и запчасти</option>
				<option>Бытовая техника</option>	
				<option>Государство и общество</option>
				<option>Деловые услуги</option>	
				<option>Игрушки, товары для детей</option>
				<option>Коммуникации, связь, системная интеграция</option>
				<option>Культура, искусство, образование, СМИ</option>
				<option>Медицина, красота и здоровье</option>
			</select>
			
			<div class="date_select">
				<select class="multi_date" multiple="multiple" name="">
					<option>2001</option>							
					<option>2002</option>
					<option>2003</option>
					<option>2004</option>
					<option>2005</option>
					<option>2006</option>
				</select>
			</div>
		</form>
				
		<ul class="tag_list">
			<li>Банки и финансы<span class="case-quantity">12</span><span class="icon-cancel"></span></li>
		</ul>

		<div class="portfolio_list flip_list clearfix">
			<div class="row clearfix">
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/tmp/portfolio_pic1.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS </h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо» Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>			<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
				<div class="item">
					<a href="#" class="link"></a>
					<h3 class="title">CROCUS CITY HALL CROCUS CITY HALL</h3>
					<p>Крокус Сити Холл — это уникальный двухуровневый концертный зал, расположенный в 3-м павильоне МВЦ «Крокус Экспо»</p>
					<span>Дизайн, интеграция, 1С, онлайн-продажа</span>
					<div class="picture"><img src="images/noImg_240x180.jpg" alt=""></div>
				</div>
			</div>
		</div>

		<div class="bottom_block"></div>
	</div>

</section>

<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/footer.php");?>   


