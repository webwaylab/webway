<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/header.php");?>

<!-- Link Swiper's -->
<link rel="stylesheet" href="/js/swiper/swiper.min.css">
<script src="/js/swiper/swiper.min.js"></script>

<section class="main">
	<div class="main_swiper">
		<!-- Swiper -->

	 	<div class="swiper-container gallery-thumbs">
	        <div class="swiper-wrapper">
	            <div class="swiper-slide item1"></div>
	            <div class="swiper-slide item2"></div>
	            <div class="swiper-slide item3"></div>
	            <div class="swiper-slide item4"></div>
	            <div class="swiper-slide item5"></div>
	            <div class="swiper-slide item6"></div>
	            <div class="swiper-slide item7"></div>
	        </div>
	    </div>

		<div class="swiper-container gallery-top">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Новые супер-проекты</h3>
					<div class="airship" style="background-image:url('images/airship1.png');" data-swiper-parallax="-1200"></div>
					<div class="description" data-swiper-parallax="-2500">
						<span class="title_mobile">Новые супер-проекты</span>
						<p>Мы очень увлечены своей работой, не умеем делать плохо и отдаем её только тем, кто ее ценит и еще немного текста про этот набор проектов ниже. Они являются ссылками на кейсы</p>
					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/tmp/main-project1.jpg" alt="" /><span class="title">Бабушкино <br>лукошко</span></a></li>
						<li><a href="#"><img src="images/tmp/main-project2.jpg" alt="" /><span class="title">Теремок</span></a></li>
						<li><a href="#"><img src="images/tmp/main-project3.jpg" alt="" /><span class="title">Органик<br>маркет</span></a></li>
						<li><a href="#"><img src="images/tmp/main-project4.jpg" alt="" /><span class="title"><strong>924</strong>клиента<br>к 2016 году</span></a></li>
						<li><a href="#"><img src="images/tmp/main-project5.jpg" alt="" /><span class="title"><strong>28</strong>наград<br>за 15 лет</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>
					</ul>
				</div>
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Поддержка и развитие</h3>
					<div class="airship support" style="background-image:url('images/support.png');" data-swiper-parallax="-1200"></div>
					<div class="text description" data-swiper-parallax="-2500">
						<span class="title_mobile">Поддержка и развитие</span>
						<p>SLA 24*7 <br>43 клиента на постоянной поддержке</p>
					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">М.Видео</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Дикси</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Газпром</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title long">Евразийский <br>Банк <br>Развития</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Ланит</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Монарх</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title long">Центральная <br>Клиническая <br>Больница</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>

					</ul>
				</div>
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Электронная коммерция</h3>
					<div class="airship e_commerce" style="background-image:url('images/e_commerce.png');" data-swiper-parallax="-1200"></div>
					<div class="text description" data-swiper-parallax="-2500">
						<span class="title_mobile">Электронная коммерция</span>
						<p>ТОП10 разработчиков интернет-магазинов <br>Оборот более 300 000 000 рублей в месяц</p>

					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Крокус <br>Групп</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Русклимат</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Мегги Молл</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Электромонтаж</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Торис</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Органик <br>маркет</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>
					</ul>
				</div>
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Решения для бизнеса</h3>
					<div class="airship business_solutions" style="background-image:url('images/business_solutions.png');" data-swiper-parallax="-1200"></div>
					<div class="text description" data-swiper-parallax="-2500">
						<span class="title_mobile">Решения для бизнеса</span>
						<p>Текст</p>
					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Информационные <br>порталы </span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Корпоративные <br>порталы </span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">B2B-системы</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title long">Корпоративные <br>сервисы <br>(Алмаз-Антей)</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title long">Интеграция с <br>учетными <br>системами</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>
					</ul>				
				</div>
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Отраслевые решения</h3>
					<div class="airship industry_solutions" style="background-image:url('images/industry_solutions.png');" data-swiper-parallax="-1200"></div>
					<div class="text description" data-swiper-parallax="-2500">
						<span class="title_mobile">Отраслевые решения</span>
						<p>Текст</p>
					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Девелоперы и <br>застройщики </span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Концертные <br>площадки</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Оптовые <br>продажи </span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Государство</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Для детей</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Отели и <br>гостиницы</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Ювелирные <br>изделия</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>
					</ul>				
				</div>
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Адаптив и mobile</h3>
					<div class="airship adaptive_mobile" style="background-image:url('images/adaptive_mobile.png');" data-swiper-parallax="-1200"></div>
					<div class="text description" data-swiper-parallax="-2500">
						<span class="title_mobile">Адаптив и mobile</span>
						<p>Текст</p>

					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Теремок</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Рэдиссон</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">ЦКБ</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Вегас Лекс</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Прайдекс</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Домстрой</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>
					</ul>				
				</div>
				<div class="swiper-slide">
					<h3 class="title" data-swiper-parallax="-100">Компетенции</h3>
					<div class="airship competencies" style="background-image:url('images/competencies.png');" data-swiper-parallax="-1200"></div>
					<div class="text description" data-swiper-parallax="-2500">
						<span class="title_mobile">Компетенции</span>
						<p>Текст</p>
					</div>
					<ul class="case_list">
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Креатив</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Проектирование</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Разработка</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Highload</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Адаптив</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Поддержка</span></a></li>
						<li><a href="#"><img src="images/noImg.jpg" alt="" /><span class="title">Веб-интеграция</span></a></li>
						<li>
							<div class="last">
								<img src="images/main-project.png" alt="" />
								<span class="title">займи свое место</span>
							</div>
						</li>
					</ul>				
				</div>
			</div>

			<!-- Add Navigation -->
			<div class="swiper-button-prev swiper-button-white"></div>
			<div class="swiper-button-next swiper-button-white"></div>
		</div>

	</div>
</section>

<script src="js/main_slider.js"></script>

<? include_once($_SERVER["DOCUMENT_ROOT"]."/v/include/footer.php");?>   


