<? include_once($_SERVER["DOCUMENT_ROOT"]."/new_site/include/header.php");?>

<style>body {background: #818181;}</style>

<section class="main">
	<ul class="breadcrambs">
		<li><a href="#">webway</a></li>/
		<li>направления</li>
	</ul> 	
	<h1 class="section_title">направления</h1>
	<div class="clearfix main_block_brick">
		<a href="#" class="big first">
			<span class="disk animated animatedDisk"></span>  
			<span class="title">
				<span>WEB-РАзработка</span>
				Сайты: Самые разные / Простые и сложные / Большие и маленькие / Имиджевые и продающие / Строгие и сумасшедшие / Для крупных заказчиков и тех, кто только начинает бизнес
			</span>
		</a>
		<a href="#" style="background-image: url('images/tmp/directions-2.jpg');">
			<span class="title">
				<span>АДАПТИВ</span>
				Сайты: Самые разные / Простые и сложные / Большие и маленькие / Имиджевые и продающие / Строгие и сумасшедшие / Для крупных заказчиков и тех, кто только начинает бизнес
			</span>
		</a>
		<a href="#" style="background-image: url('images/tmp/directions-3.jpg');">
			<span class="title">
				<span>Интеграция</span>
				Сайты: Самые разные / Простые и сложные / Большие и маленькие / Имиджевые и продающие / Строгие и сумасшедшие 
			</span>
		</a>
		<a href="#" class="big" style="background-image: url('images/tmp/directions-4.jpg');">
			<span class="title">
				<span>Электронная коммерция</span>
				Сайты: Самые разные / Простые и сложные / Большие и маленькие / Имиджевые и продающие / Строгие и сумасшедшие / Для крупных заказчиков и тех, кто только начинает бизнес
			</span>
		</a>
	</div>

</section>

<? include_once($_SERVER["DOCUMENT_ROOT"]."/new_site/include/footer.php");?>   


