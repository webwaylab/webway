<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" >
	<title>Webway</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

	<link rel="stylesheet" href="css/reset.css" />
	<link rel="stylesheet" href="css/font-awesome.css" />
	<link rel="stylesheet" type="text/css" href="css/animate.css"> 

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>

	<link rel="stylesheet" type="text/css" href="js/multiselect/jquery.multiselect.css" />
	<script type="text/javascript" src="js/multiselect/jquery.multiselect.js"></script>	

	<script src="js/masonry.pkgd.min.js"></script>

	<link rel="stylesheet" href="css/style.css" />

	<script type="text/javascript" src="js/script.js"></script>

</head>

<body>

<div class="header-wrap">
	<header class="header clearfix">
		<div class="left">
			<a href="#header_menu" class="menu-link header_menu_button popup-with-move-anim" id="menu-opener" alt="menu"><span>Разделы</span> <i class="icon-menu"></i></a>      

			<div class="logo"><a href="/" alt="webway">webway</a></div>

			<span class="header_text">17 лет в пути</span>
		</div>
		<div class="right">
			<form action="" class="header_search">
				<input type="text" placeholder="Поиск" />
				<input type="submit" class="icon-search" value="искать" />
			</form>
			<a href="" class="header_social fb" title=""><i class="icon-facebook-squared"></i></a>
			<a href="" class="header_location"><i class="icon-location"></i></a>
		</div>          
	</header>  <!-- //END HEADER BLOCK -->
</div>

<div id="header_menu" class="white-popup-block header_menu-wrap">
	<div class="header_menu_wrapper">
		<div class="header_menu_container">
			<div class="header_menu_overflow">
				<div class="mobile_only">
					<form action="" class="header_search">
						<input type="text" placeholder="Поиск" />
						<input type="submit" class="icon-search" value="искать" />
					</form>
				</div>
				<ul>
					<li><a href="/">Проекты<span>924</span></a></li>
					<li><a href="/#services">Новости</a></li>
					<li><a href="/#events">Услуги</a></li>
					<li><a href="/#about">Ваш проект</a></li>
				</ul>
				<ul>
					<li><a href="/">Клиенты довольны</a></li>
					<li><a href="/#services">Документы</a></li>
					<li><a href="/#events">Контакты</a></li>
					<li><a href="/#about">Мы в facebook</a></li>
				</ul>
				<p><i class="icon-location-2"></i>Москва, Щелковское шоссе д.2, ст. м. Черкизовская, Преображенская площадь</p>
			</div>
		</div>
	</div>
</div>  