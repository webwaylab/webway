$(document).ready(function(){

		var h = $(window).height();
		if($(window).width() > 768){
			$("#section1 .post").addClass('animated zoomIn');
			$(window).scroll(function(){
				if ( ($(this).scrollTop()+h - 400) >= $("#section2").offset().top) {
					$("#section2 .post").css({visibility:"visible"});
					$("#section2 .post").addClass('animated zoomIn');
				} 
				if ( ($(this).scrollTop()+h - 600) >= $("#section3").offset().top) {
					$("#section3 .post").css({visibility:"visible"});
					$("#section3 .post").addClass('animated zoomIn');
				} 
				if ( ($(this).scrollTop()+h - 600) >= $("#section4").offset().top) {
					$("#section4 .post").css({visibility:"visible"});
					$("#section4 .post").eq(0).addClass('animated bounceInLeft');
					$("#section4 .post").eq(1).addClass('animated bounceInRight');
					$("#section4 .post").eq(2).addClass('animated bounceInLeft');
					$("#section4 .post").eq(3).addClass('animated bounceInRight');
				} 
				if ( $(this).scrollTop() == 0 ) {
					$("#section2 .post, #section3 .post, #section4 .post").each(function(){
						if ( $(this).hasClass('last') ) {
							$(this).removeClass().addClass('post last');
						} else {
							$(this).removeClass().addClass('post');
						}
						$(this).css({visibility:"hidden"});
					});
				}
			});
		}

		$(".lines_block span").hide();
		setTimeout(function () { $('.line1').show().addClass('animated fadeInDown');}, 1000 );	
		setTimeout(function () { $('.line2').show().addClass('animated fadeInDown');}, 1200 );	
		setTimeout(function () { $('.line3').show().addClass('animated fadeInDown');}, 1400 );	
		setTimeout(function () { $('.line4').show().addClass('animated fadeInDown');}, 1600 );	
		setTimeout(function () { $('.line5').show().addClass('animated fadeInDown');}, 1800 );	
		setTimeout(function () { $('.line6').show().addClass('animated fadeInDown');}, 2000 );	
		
	});	