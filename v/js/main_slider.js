$(document).ready(function(){
	$('body').attr('data-item', 1);
	$('body[data-item="1"]').addClass('item1');

	var galleryThumbs = new Swiper('.gallery-thumbs', {	loop: true });

	var galleryTop = new Swiper('.swiper-container.gallery-top', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		parallax: true,
		speed: 900,
		loop:true
	});	

	galleryTop.on('onSlideChangeStart', function (swiper) {
	  	var sindex = $('.gallery-top .swiper-slide-active').data("swiper-slide-index");
	 	$('body').attr('data-item', sindex + 1);

		$('body[data-item="1"]').removeClass('item7 item2').addClass('item1');
		$('body[data-item="2"]').removeClass('item1 item3').addClass('item2');
		$('body[data-item="3"]').removeClass('item2 item4').addClass('item3');
		$('body[data-item="4"]').removeClass('item3 item5').addClass('item4');
		$('body[data-item="5"]').removeClass('item4 item6').addClass('item5');
		$('body[data-item="6"]').removeClass('item5 item7').addClass('item6');
		$('body[data-item="7"]').removeClass('item6 item1').addClass('item7');				
	});

	galleryTop.params.control = galleryThumbs;
	galleryThumbs.params.control = galleryTop;
});	