<? include_once($_SERVER["DOCUMENT_ROOT"]."/new_site/include/header.php");?>

<style>body {background: #818181;}</style>

<section class="main">
	<ul class="breadcrambs">
		<li><a href="#">webway</a></li>/
		<li>о компании</li>
	</ul> 	
	<h1 class="section_title center">Мы ближе, чем вы думаете</h1>

	<script src='https://maps.googleapis.com/maps/api/js?v=3.exp'></script>

	<div class="googlemap"><div id="gmap_canvas"></div></div>

	<script type='text/javascript'>
		function init_map(){
			var myOptions = { zoom:15, center:new google.maps.LatLng(55.80282791904125,37.758326846032794), mapTypeId: google.maps.MapTypeId.ROADMAP };
			map = new google.maps.Map(document.getElementById('gmap_canvas'), myOptions);
			marker = new google.maps.Marker({ map: map,	position: new google.maps.LatLng(55.80282791904125,37.758326846032794), icon: { url: "images/marker.png", scaledSize: new google.maps.Size(49, 75) }
			});
			google.maps.event.addListener(marker, 'click', function(){ infowindow.open(map,marker);	});
		}
		google.maps.event.addDomListener(window, 'load', init_map);
	</script>

	<div class="feedback two_col">
		<div class="contacts col">
			<span class="phone">+7 495 231-16-90</span>
			<p>Москва, Щелковское шоссе д.2, ст. м. Черкизовская, <br>Преображенская площадь.</p>
			<p><span>Для вопросов и предложений </span><a href="mailto:clients@webway.ru">clients@webway.ru</a></p>
			<p><span>Техническая поддержка </span><a href="mailto:support@webway.ru">support@webway.ru</a></p>
			<p><span>Для вакансий </span><a href="mailto: hr@webway.ru">hr@webway.ru</a></p>
		</div>
		<div class="col">
			<form id="contact" action="" method="post">
				<h3>Обратная связь</h3>
				<fieldset>
					<input placeholder="Ваше имя" type="text" tabindex="1" required autofocus>
				</fieldset>
				<fieldset>
					<input placeholder="Телефон" type="tel" tabindex="2" required>
				</fieldset>
				<fieldset>
					<input placeholder="Эл. почта" type="email" tabindex="3" required>
				</fieldset>
				<fieldset>
					<textarea placeholder="Ваше сообщение" tabindex="5" required></textarea>
				</fieldset>
				<fieldset>
					<button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Отправить</button>
				</fieldset>
			</form>

		</div>
	</div>

</section>

<? include_once($_SERVER["DOCUMENT_ROOT"]."/new_site/include/footer.php");?>   


