<? include_once($_SERVER["DOCUMENT_ROOT"]."/new_site/include/header.php");?>

<style>body {background: #818181;}</style>

<script type="text/javascript" src="js/about.js"></script>

<section class="main">
	<ul class="breadcrambs">
		<li><a href="#">webway</a></li>/
		<li>о компании</li>
	</ul> 	
	<h1 class="section_title center">WEBWAY — Наш с вами путь</h1>
	<div class="clearfix white_container">
		<section id="section1">
			<ul class="about_list">
				<li class="item1">
					<div class="post">
						<span class="title">1999 старт</span>
						<p>С 1999 года проектируем, разрабатываем, поддерживаем и развиваем просто сайты, интернет-магазины,<br> b2b-системы, а также очень сложные и большие проекты, адаптивно, креативно, всегда бережно и тщательно.</p>
					</div>
				</li>
				<li class="item2">
					<div class="post">
						<span class="title">1400 проектов</span>
						<p>С 1999 года проектируем, разрабатываем, поддерживаем и развиваем просто сайты, интернет-магазины</p>
					</div>
				</li>
				<li class="item3">
					<div class="post">
						<span class="title">70 наград</span>
						<p>Опишите основные награды в этмо тексте. Чем больше тем лучше. Инфографика требует информации</p>
					</div>
				</li>
				<li class="item4">
					<div class="post">
						<span class="title">900 клиентов</span>
						<p>Опишите основных клиентов в этмо тексте. Чем больше тем лучше. Инфографика требует информации</p>
					</div>
				</li>
				<li class="item5">
					<div class="post">
						<span class="title">72%</span>
						<p>Заказчиков приходят к нам по рекоммендации</p>
					</div>
				</li>
				<li class="item6">
					<div class="post">
						<span class="title">56%</span>
						<p>Заказчиков возвращаются к нам за новыми решениями и новыми проектами</p>
					</div>
				</li>
				<li class="item7">
					<div class="post last">
						<span class="title">43</span>
						<p>заказчика непрерывно сотрудничаут с нами от 4-х до 14 лет</p>
					</div>
				</li>
			</ul>
			<div class="lines_block">
				<div class="inner">
					<span class="line1"></span>
					<span class="line2"></span>
					<span class="line3"></span>
					<span class="line4"></span>
					<span class="line5"></span>
					<span class="line6"></span>
				</div>
			</div>
		</section>
		<section id="section2">
			<ul class="about_list">
				<li class="item8">
					<div class="post last">
						<span class="title">мы</span>
						<p>самые заботливые, понимающие интеллигентные, клиенториентированные и (вы это уже и сами заметили) скромные разработчики на свете  :)</p>
					</div>
				</li>
			</ul>
		</section>
		<section id="section3" class="with_us">
			<div class="post"><span class="title">с нами</span></div>
		</section>
		<section id="section4">
			<ul class="about_list">
				<li class="item9">
					<div class="post">
						<span class="title">Прозрачно</span>
						<p>Вы наблюдаете и контролируете работу над вашим проектом online на любом этапе в любой момент времени.</p>
					</div>
				</li>
				<li class="item10">
					<div class="post">
						<span class="title">КОМФОРТНО</span>
						<p>Приедем и привезем, расскажем и покажем, проконсультируем и ответим на любые вопросы.</p>
					</div>
				</li>
				<li class="item11">
					<div class="post">
						<span class="title">Выгодно</span>
						<p>Отлаженная система бережливого производства без превышения бюджета и растраты ваших средств.</p>
					</div>
				</li>
				<li class="item12">
					<div class="post last">
						<span class="title">Оперативно</span>
						<p>Всегда найдем профессиональные руки и светлые головы для ваших задач.</p>
					</div>
				</li>
			</ul>


		</div>
	</section>

	<? include_once($_SERVER["DOCUMENT_ROOT"]."/new_site/include/footer.php");?>   


