<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О нас");
$APPLICATION->AddHeadScript('/js/about.js');
?><style>body {background: #818181;}</style>
<h1 class="section_title center">WEBWAY — Наш с вами путь</h1>
<div class="clearfix white_container">
 <section id="section1">
	<ul class="about_list">
		<li class="item1">
		<div class="post">
 <span class="title">1999 старт</span>
			<p>
				 С 1999 года&nbsp;проектируем, разрабатываем, поддерживаем и развиваем интернет-проекты, адаптивно, креативно, всегда бережно и тщательно
			</p>
		</div>
 </li>
		<li class="item2">
		<div class="post">
 <span class="title">1400 проектов</span>
			<p>
				 больших и маленьких, простых и сложных, имиджевых и продающих, строгих и сумасшедших для бизнеса любого размера
			</p>
		</div>
 </li>
		<li class="item3">
		<div class="post">
 <span class="title">70 наград</span>
			<p>
				 Премия Рунета, Рейтинг Рунета, Золотой Сайт, КМФР, Propeller Digital и еще много-много наград и дипломов
			</p>
		</div>
 </li>
		<li class="item4">
		<div class="post">
 <span class="title">900 клиентов</span>
			<p>
				 довольны своими проектами и обращаются к нам снова и снова
			</p>
		</div>
 </li>
		<li class="item5">
		<div class="post">
 <span class="title">72%</span>
			<p>
				 заказчиков приходят к нам по рекомендации
			</p>
		</div>
 </li>
		<li class="item6">
		<div class="post">
 <span class="title">56%</span>
			<p>
				 заказчиков возвращаются к нам за новыми решениями и новыми проектами
			</p>
		</div>
 </li>
		<li class="item7">
		<div class="post last">
 <span class="title">43</span>
			<p>
				 заказчика непрерывно сотрудничают с нами от 4-х до 14 лет
			</p>
		</div>
 </li>
	</ul>
	<div class="lines_block">
		<div class="inner">
 <span class="line1"></span> <span class="line2"></span> <span class="line3"></span> <span class="line4"></span> <span class="line5"></span> <span class="line6"></span>
		</div>
	</div>
 </section> <section id="section2">
	<ul class="about_list">
		<li class="item8">
		<div class="post last">
 <span class="title">мы</span>
			<p>
				 самые заботливые, понимающие интеллигентные, клиенториентированные и (вы это уже и сами заметили) скромные разработчики на свете :)
			</p>
		</div>
 </li>
	</ul>
 </section> <section id="section3" class="with_us">
	<div class="post">
 <span class="title">с нами</span>
	</div>
 </section> <section id="section4">
	<ul class="about_list">
		<li class="item9">
		<div class="post">
 <span class="title">Прозрачно</span>
			<p>
				 Вы наблюдаете и контролируете работу над вашим проектом online на любом этапе в любой момент времени.
			</p>
		</div>
 </li>
		<li class="item10">
		<div class="post">
 <span class="title">КОМФОРТНО</span>
			<p>
				 Приедем и привезем, расскажем и покажем, проконсультируем и ответим на любые вопросы.
			</p>
		</div>
 </li>
		<li class="item11">
		<div class="post">
 <span class="title">Выгодно</span>
			<p>
				 Отлаженная система бережливого производства без превышения бюджета и растраты ваших средств.
			</p>
		</div>
 </li>
		<li class="item12">
		<div class="post last">
 <span class="title">Оперативно</span>
			<p>
				 Всегда найдем профессиональные руки и светлые головы для ваших задач.
			</p>
		</div>
 </li>
	</ul>
 </section>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>