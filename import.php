<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Webway");
$data=json_decode(file_get_contents('http://webway2017.alfa.webway.ru/export.php'),true);
CModule::IncludeModule('iblock');
$arConfig=array(
	'IBLOCK_PROJECTS'=>3,
	'IBLOCK_TYPES'=>4,
	'IBLOCK_BRANCHS'=>5,
	'IBLOCK_YEARS'=>8,
	'IBLOCK_CLIENTS'=>9
	);
p('Справочник отраслей');
$times=array(
	'start'=>date('Y-m-d H:i:s'),
	'stop'=>'');
$errors=array();
$counts=array(
	'Added'=>0,
	'Updated'=>0,
	'Deleted'=>0,
	'Error'=>0
	);
$arResult['BRANCHS']=[];
$made_branches=[];
$Ob=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arConfig['IBLOCK_BRANCHS']),false,false,array("ID","NAME","XML_ID"));
while($arBranch=$Ob->GetNext()){
	$made_branches[$arBranch["XML_ID"]]=$arBranch['ID'];
	$arResult['BRANCHS'][$arBranch["XML_ID"]]=$arBranch['ID'];
}
foreach ($data['BRANCHS'] as $k => $v) {
	$el = new CIBlockElement;
	$arFields=array(
		"IBLOCK_ID" => $arConfig['IBLOCK_BRANCHS'],
		"XML_ID" => $v["XML_ID"],
		"NAME" => $v["NAME"],
		"SORT" => $v["SORT"],
		"CODE"=>$v['CODE']
		//"CODE" => Cutil::translit($pricebushe["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
	);
	
	if($made_branches[$v["XML_ID"]]==''){
		if($PRODUCT_ID = $el->Add($arFields)) {
			$counts['Added']++;
			$arResult['BRANCHS'][$v["XML_ID"]]=$PRODUCT_ID;
		} else {
			$counts['Error']++;
			$errors[]="Error Element add: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
	}else{
		if($el->Update($made_branches[$v["XML_ID"]], $arFields)){
			$counts['Updated']++;
		}else{
			$counts['Error']++;
			//p($arLoadProductArray);
			$errors[]="Error Element update: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
		unset($made_branches[$v["XML_ID"]]);
	}
}
if(is_array($made_branches)){
	foreach ($made_branches as $k => $v) {
		CIBlockElement::Delete($v);
		$counts['Deleted']++;
	}
}
$times['stop']=date('Y-m-d H:i:s');

p($counts);
p($errors);
p($times);
p('Справочник направлений');
$times=array(
	'start'=>date('Y-m-d H:i:s'),
	'stop'=>'');
$errors=array();
$counts=array(
	'Added'=>0,
	'Updated'=>0,
	'Deleted'=>0,
	'Error'=>0
	);
$arResult['TYPES']=[];
$made_types=[];
$Ob=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arConfig['IBLOCK_TYPES']),false,false,array("ID","NAME","XML_ID"));
while($arType=$Ob->GetNext()){
	$made_types[$arType["XML_ID"]]=$arType['ID'];
	$arResult['TYPES'][$arType["XML_ID"]]=$arType['ID'];
}
foreach ($data['TYPES'] as $k => $v) {
	$el = new CIBlockElement;
	$arFields=array(
		"IBLOCK_ID" => $arConfig['IBLOCK_TYPES'],
		"XML_ID" => $v["XML_ID"],
		"NAME" => $v["NAME"],
		"SORT" => $v["SORT"],
		"CODE"=>$v['CODE']
		//"CODE" => Cutil::translit($pricebushe["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
	);
	
	if($made_types[$v["XML_ID"]]==''){
		if($PRODUCT_ID = $el->Add($arFields)) {
			$counts['Added']++;
			$arResult['TYPES'][$v["XML_ID"]]=$PRODUCT_ID;
		} else {
			$counts['Error']++;
			$errors[]="Error Element add: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
	}else{
		if($el->Update($made_types[$v["XML_ID"]], $arFields)){
			$counts['Updated']++;
		}else{
			$counts['Error']++;
			//p($arLoadProductArray);
			$errors[]="Error Element update: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
		unset($made_types[$v["XML_ID"]]);
	}
}
if(is_array($made_types)){
	foreach ($made_types as $k => $v) {
		CIBlockElement::Delete($v);
		$counts['Deleted']++;
	}
}
$times['stop']=date('Y-m-d H:i:s');

p($counts);
p($errors);
p($times);
p('Справочник клиентов');
$times=array(
	'start'=>date('Y-m-d H:i:s'),
	'stop'=>'');
$errors=array();
$counts=array(
	'Added'=>0,
	'Updated'=>0,
	'Deleted'=>0,
	'Error'=>0
	);
$arResult['CLIENTS']=[];
$made_clients=[];
$Ob=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arConfig['IBLOCK_CLIENTS']),false,false,array("ID","NAME","XML_ID"));
while($arClient=$Ob->GetNext()){
	$made_clients[$arClient["XML_ID"]]=$arClient['ID'];
	$arResult['CLIENTS'][$arClient["XML_ID"]]=$arClient['ID'];
}
foreach ($data['CLIENTS'] as $k => $v) {
	$el = new CIBlockElement;
	$arFields=array(
		"IBLOCK_ID" => $arConfig['IBLOCK_CLIENTS'],
		"XML_ID" => $v["XML_ID"],
		"NAME" => $v["NAME"],
		"SORT" => $v["SORT"],
		"DETAIL_TEXT"=>$v['DATA']['DESCRIPTION'],
		"CODE"=>$v['CODE'],
		"PROPERTY_VALUES"=>[]
		//"CODE" => Cutil::translit($pricebushe["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
	);
	foreach ($v['DATA']['BRANCHS'] as $k2 => $v2) {
		$arFields['PROPERTY_VALUES']['BRANCHS'][]=$arResult['BRANCHS'][$v2];
	}
	if($made_clients[$v["XML_ID"]]==''){
		if($PRODUCT_ID = $el->Add($arFields)) {
			$counts['Added']++;
			$arResult['CLIENTS'][$v["XML_ID"]]=$PRODUCT_ID;
		} else {
			$counts['Error']++;
			$errors[]="Error Element add: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
	}else{
		if($el->Update($made_clients[$v["XML_ID"]], $arFields)){
			$counts['Updated']++;
		}else{
			$counts['Error']++;
			//p($arLoadProductArray);
			$errors[]="Error Element update: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
		unset($made_clients[$v["XML_ID"]]);
	}
}
if(is_array($made_clients)){
	foreach ($made_clients as $k => $v) {
		CIBlockElement::Delete($v);
		$counts['Deleted']++;
	}
}
$times['stop']=date('Y-m-d H:i:s');

p($counts);
p($errors);
p($times);
p('Справочник годов');
$times=array(
	'start'=>date('Y-m-d H:i:s'),
	'stop'=>'');
$errors=array();
$counts=array(
	'Added'=>0,
	'Updated'=>0,
	'Deleted'=>0,
	'Error'=>0
	);

foreach ($data['PROJECTS'] as $k => $v) {
	$YEARS[$v['DATA']['YEAR']]++;
}
//p($YEARS);
$made_years=[];
$Ob=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arConfig['IBLOCK_YEARS']),false,false,array("ID","NAME","XML_ID"));
while($arYear=$Ob->GetNext()){
	$made_years[$arYear["XML_ID"]]=$arYear['ID'];
	$arResult['YEARS'][$arYear["XML_ID"]]=$arYear['ID'];
}
foreach ($YEARS as $k => $v) {
	$el = new CIBlockElement;
	$arFields=array(
		"IBLOCK_ID" => $arConfig['IBLOCK_YEARS'],
		"XML_ID" => $k,
		"NAME" => $k,
		"SORT" => intval($k),
		"CODE"=>$k
		//"CODE" => Cutil::translit($pricebushe["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
	);
	
	if($made_years[$v["XML_ID"]]==''){
		if($PRODUCT_ID = $el->Add($arFields)) {
			$counts['Added']++;
			$arResult['YEARS'][$arFields['XML_ID']]=$PRODUCT_ID;
		} else {
			$counts['Error']++;
			$errors[]="Error Element add: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
	}else{
		if($el->Update($made_years[$arFields["XML_ID"]], $arFields)){
			$counts['Updated']++;
		}else{
			$counts['Error']++;
			//p($arLoadProductArray);
			$errors[]="Error Element update: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
		unset($made_years[$arFields["XML_ID"]]);
	}
}
if(is_array($made_years)){
	foreach ($made_years as $k => $v) {
		CIBlockElement::Delete($v);
		$counts['Deleted']++;
	}
}
$times['stop']=date('Y-m-d H:i:s');

p($counts);
p($errors);
p($times);
p('Справочник проектов');
$times=array(
	'start'=>date('Y-m-d H:i:s'),
	'stop'=>'');
$errors=array();
$counts=array(
	'Added'=>0,
	'Updated'=>0,
	'Deleted'=>0,
	'Error'=>0
	);
$arResult['PROJECTS']=[];
$made_projects=[];
$Ob=CIBlockElement::GetList(array(),array("IBLOCK_ID"=>$arConfig['IBLOCK_PROJECTS']),false,false,array("ID","NAME","XML_ID"));
while($arProject=$Ob->GetNext()){
	$made_projects[$arProject["XML_ID"]]=$arProject['ID'];
	$arResult['PROJECTS'][$arProject["XML_ID"]]=$arProject['ID'];
}
$arrfiles=array('png','jpg','jpeg');
foreach ($data['PROJECTS'] as $k => $v) {
	$el = new CIBlockElement;
	$arFields=array(
		"IBLOCK_ID" => $arConfig['IBLOCK_PROJECTS'],
		"XML_ID" => $v["XML_ID"],
		"NAME" => ($v['DATA']["NAME_CL"]!='')?$v['DATA']["NAME_CL"]:$v["NAME"],
		"SORT" => $v["SORT"],
		"PREVIEW_TEXT"=>$v['DATA']['DESCRIPTION'],
		"DETAIL_TEXT"=>$v['DATA']['FDESCRIPTION'],
		"CODE"=>$v['CODE'],
		"PROPERTY_VALUES"=>[]
		//"CODE" => Cutil::translit($pricebushe["Name"],"ru",array("replace_space"=>"-","replace_other"=>"-")),
	);
	$arFields['PROPERTY_VALUES']['TYPE']=3;
	$arFields['PROPERTY_VALUES']['LINKS'][]=$v['DATA']['URL'];
	$arFields['PROPERTY_VALUES']['YEAR']=$arResult['YEARS'][$v['DATA']['YEAR']];
	$arFields['PROPERTY_VALUES']['CLIENT']=$arResult['CLIENTS'][$v['DATA']['CLIENT']];
	$arFields['PROPERTY_VALUES']['SROK']="Сотрудничество с ".$v['DATA']['YEAR'];
	foreach ($v['DATA']['BRANCHS'] as $k2 => $v2) {
		$arFields['PROPERTY_VALUES']['OTRASLI'][]=$arResult['BRANCHS'][$v2];
	}
	foreach ($v['DATA']['TYPES'] as $k2 => $v2) {
		$arFields['PROPERTY_VALUES']['NAPRAV'][]=$arResult['TYPES'][$v2];
	}
	
	foreach ($v['IMAGES'] as $k2 => $v2) {
		$path=false;
		foreach ($arrfiles as $k3 => $v3) {
			if(file_exists($_SERVER['DOCUMENT_ROOT'].$v2['PATH'].".".$v3)){
				$path=true;

				$v2['PATH']=$_SERVER['DOCUMENT_ROOT'].$v2['PATH'].".".$v3;
			}
		}
		if($path){
			$arFields['PROPERTY_VALUES']['SCREENS'][$k2]=array('VALUE'=>CFile::MakeFileArray($v2['PATH']),'DESCRIPTION'=>$v2['NAME']);
		}
	}
	if(is_array($arFields['PROPERTY_VALUES']['SCREENS']['0'])){
		$arFields['PREVIEW_PICTURE']=$arFields['PROPERTY_VALUES']['SCREENS']['n0']['VALUE'];
	}
	if($made_projects[$v["XML_ID"]]==''){
		if($PRODUCT_ID = $el->Add($arFields)) {
			$counts['Added']++;
			$arResult['PROJECTS'][$arFields['XML_ID']]=$PRODUCT_ID;
		} else {
			$counts['Error']++;
			$errors[]="Error Element add: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
	}else{
		if($el->Update($made_projects[$arFields["XML_ID"]], $arFields)){
			$counts['Updated']++;
		}else{
			$counts['Error']++;
			//p($arLoadProductArray);
			$errors[]="Error Element update: ".$arFields['NAME']." ".$el->LAST_ERROR;
		}
		unset($made_projects[$v["XML_ID"]]);
	}
}
if(is_array($made_projects)){
	foreach ($made_projects as $k => $v) {
		CIBlockElement::Delete($v);
		$counts['Deleted']++;
	}
}
$times['stop']=date('Y-m-d H:i:s');

p($counts);
p($errors);
p($times);
?>

<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");
?>