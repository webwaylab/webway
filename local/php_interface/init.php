<?
function p($v){
	echo "<pre>";print_r($v);echo "</pre>";
}
function birthday($sec_birthday,$timep = 0)
{
	$sec_now = time();

	for($time = $sec_birthday, $month = 0; 
	    $time < $sec_now; 
	    $time = $time + date('t', $time) * 86400, $month++){
	    $rtime = $time;
	    }
	$month = $month - 1;

	$year = intval($month / 12);

	$month = $month % 12;
	$day = intval(($sec_now - $rtime) / 86400);
	
	$result = declination($year, "год", "года", "лет")." ";
	if($timep>0){
		$result .= declination($month, "месяц", "месяца", "месяцев")." ";
	}
	if($timep>1){
		$result .= declination($day, "день", "дня", "дней")." ";
	}
	
	return $result;
}

function declination($num, $one, $ed, $mn, $notnumber = false)
{  
	// $one="статья";  
	// $ed="статьи";  
	// $mn="статей";  
	if($num === "") print "";
	if(($num == "0") or (($num >= "5") and ($num <= "20")) or preg_match("|[056789]$|",$num))
	  if(!$notnumber)
	    return "$num $mn";
	  else
	    return $mn;
	if(preg_match("|[1]$|",$num))
	  if(!$notnumber)
	    return "$num $one";
	  else
	    return $one;
	if(preg_match("|[234]$|",$num))
	  if(!$notnumber)
	    return "$num $ed";
	  else
	    return $ed;
}
define('BIRTHDAY_YEARS',birthday(mktime(0, 0, 0, 9, 1, 1999)));
?>