<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'LISTDIRECTIONS';
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
    $arSlides=[];
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE"));
    while($arFields = $res->GetNext()) {
        $arSlides[]=$arFields['ID'];
        $arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
        $arResult['DIRECTIONS'][]=$arFields;
    }


    /*d($arResult['ARTISTS']);*/
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	//file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->__folder."/style.css",$css);
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}

$this->__component->arResult = $arResult; 
?>