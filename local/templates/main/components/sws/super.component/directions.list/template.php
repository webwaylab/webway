<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<h1 class="section_title">направления</h1>

  <div class="clearfix main_block_brick">
<?
$n=0;
foreach ($arResult['DIRECTIONS'] as $k => $v) {
  if($n==0&&$k==0){?>

<a href="#" class="big first">
      <span class="disk animated animatedDisk"></span>  
      <span class="title">
        <span><?=$v['NAME']?></span>
        <?=$v['~PREVIEW_TEXT']?>
      </span>
    </a>
  <?

  }elseif($n==0) {
    ?>
<a href="#" class="big" style="background-image: url('<?=$v['PREVIEW_PICTURE']['SRC']?>');">
      <span class="title">
          <span><?=$v['NAME']?></span>
        <?=$v['~PREVIEW_TEXT']?>
      </span>
    </a>
    <?
  }else{
    ?>
   <a href="#" style="background-image: url('<?=$v['PREVIEW_PICTURE']['SRC']?>');">
      <span class="title">
          <span><?=$v['NAME']?></span>
        <?=$v['~PREVIEW_TEXT']?>
      </span>
    </a>

    <?
  }
  
  $n++;
  if($n==3){
    $n=0;
  }
}?>
</div>