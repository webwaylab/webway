<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/*$obCache_trade = new CPHPCache();
$CACHE_ID = 'LISTF'.$arParams['CODE'];
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();*/
	CModule::IncludeModule('iblock');
    
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_NAPRAV_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE"));
    while($arFields = $res->GetNext()) {
    	$arResult['NAPRAV'][$arFields['ID']]=$arFields['NAME'];
    }
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_OTRASLI_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE"));
    while($arFields = $res->GetNext()) {
    	$arResult['OTRASLI'][$arFields['ID']]=$arFields['NAME'];
    }
    $res = CIBlockSection::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_YEARS_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"),true);
    while($arFields = $res->GetNext()) {
        //p($arFields);
        $arResult['YEARS'][$arFields['ID']]['NAME']=$arFields['NAME'];
    }
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_YEARS_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE","IBLOCK_SECTION_ID"));
    while($arFields = $res->GetNext()) {
        $arResult['YEARS'][$arFields['IBLOCK_SECTION_ID']]['IDS'][]=$arFields['ID'];
    }
   
    $arFilter=array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","PROPERTY_NO_SHOW"=>false);
    $filter=false;
    if($_REQUEST['search']){
        $filter=true;
    	$arFilter[]=array(
           "LOGIC" => "OR",
           array("?NAME" => $_REQUEST['search']),
           array("?PREVIEW_TEXT" => $_REQUEST['search']),
           );
    }
    if(count($_REQUEST['napravlenia'])>0){
        $filter=true;
    	$arFilter['PROPERTY_NAPRAV']=$_REQUEST['napravlenia'];
    }
    if(count($_REQUEST['otrasl'])>0){
        $filter=true;
    	$arFilter['PROPERTY_OTRASLI']=$_REQUEST['otrasl'];

    }
    if(count($_REQUEST['year'])>0){
        $filter=true;
        $arFilter['PROPERTY_YEAR']=[];
        foreach ($_REQUEST['year'] as $k => $v) {
           $arFilter['PROPERTY_YEAR']=array_merge($arFilter['PROPERTY_YEAR'],$arResult['YEARS'][$v]['IDS']);

        }
        array_unique($arFilter['PROPERTY_YEAR']);
    }
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), $arFilter, false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SROK","PROPERTY_MAIN","PROPERTY_NAPRAV",'PROPERTY_OTRASLI','PROPERTY_YEAR','PROPERTY_SEARCH_PIC','PROPERTY_M_BG'));
    while($arFields = $res->GetNext()) {
    	$arButtons = CIBlock::GetPanelButtons(
			$arFields["IBLOCK_ID"],
			$arFields["ID"],
			0,
			array("SECTION_BUTTONS"=>true, "SESSID"=>false, "CATALOG"=>true)
		);
        if($filter){
            foreach ($arFields['PROPERTY_OTRASLI_VALUE'] as $k => $v) {
                $arResult['COUNT_OTRASLI'][$v]++;
            }
            foreach ($arFields['PROPERTY_NAPRAV_VALUE'] as $k => $v) {
                $arResult['COUNT_NAPRAV'][$v]++;
            }

        }
		$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
		if($arFields['PROPERTY_SEARCH_PIC_VALUE']){
	        $arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PROPERTY_SEARCH_PIC_VALUE']);
		}else{
	        $arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
		}

        $arResult['PROJECTS'][]=$arFields;
    }

    //p($arResult['PROJECTS']);
    /*d($arResult['ARTISTS']);*/
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	//file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->__folder."/style.css",$css);
	// writing new $arResult to cache file
/*	$obCache_trade->EndDataCache($arResult);

}*/

$this->__component->arResult = $arResult; 
?>