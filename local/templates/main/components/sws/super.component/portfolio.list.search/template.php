<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<script src="/js/multiselect.js"></script> 
<script src="/js/viewportchecker.js"></script> 
<script src="/js/flip_list.js"></script> 

<div class="white_container clearfix">
<div class="portfolio_nav">
      <a href="/portfolio/">Лучшие проекты</a>
      <a href="/portfolio/search/"  class="active">Проекты по направлениям</a>
    </div>
    </div>
 <form id="filter" action="" >
<div class="clearfix">

    <div action="" class="portfolio_search icon-search-2">
      <input name="search" type="search" autocomplete="off" placeholder="Поиск..." value="<?=$_REQUEST['search']?>" />
      <input type="submit" class="icon-search" value="искать" />
    </div>
  </div>

  <div class="white_container clearfix">

   <div class="filter_form">
      Кейсы:
      <select class="multi" multiple="multiple" name="napravlenia[]">
        <?foreach ($arResult['NAPRAV'] as $k => $v) {?>
          <option <?=(in_array($k,$_REQUEST['napravlenia']))?'selected':'';?> value="<?=$k?>"><?=$v?></option>
        <?}?>
      </select>
       <select class="multi_otr" multiple="multiple" name="otrasl[]">
        <?foreach ($arResult['OTRASLI'] as $k => $v) {?>
          <option <?=(in_array($k,$_REQUEST['otrasl']))?'selected':'';?> value="<?=$k?>"><?=$v?></option>
        <?}?>
      </select>
      <div class="date_select">
        <select class="multi_date" multiple="multiple" name="year[]">
          <?foreach ($arResult['YEARS'] as $k => $v) {?>
          <option <?=(in_array($k,$_REQUEST['year']))?'selected':'';?> value="<?=$k?>"><?=$v['NAME']?></option>
        <?}?>
        </select>
      </div>
        </div>

    <ul class="tag_list">
      <?foreach ($_REQUEST['napravlenia'] as $k => $v) {?>
        <li><?=$arResult['NAPRAV'][$v]?><span class="case-quantity"><?=$arResult['COUNT_NAPRAV'][$v]?></span><a href="<?=str_replace('&napravlenia%5B%5D='.$v,'',$_SERVER['REQUEST_URI'])?>" class="icon-cancel"></a></li>  
      <?}?>
            <?foreach ($_REQUEST['otrasl'] as $k => $v) {?>
        <li><?=$arResult['OTRASLI'][$v]?><span class="case-quantity"><?=$arResult['COUNT_OTRASLI'][$v]?></span><a href="<?=str_replace('&otrasl%5B%5D='.$v,'',$_SERVER['REQUEST_URI'])?>" class="icon-cancel"></a></li>  
      <?}?>
       <?foreach ($_REQUEST['year'] as $k => $v) {?>
        <li><?=$arResult['YEARS'][$v]['NAME']?><span class="case-quantity"><?=$arResult['COUNT_YEARS'][$v]?></span><a href="<?=str_replace('&year%5B%5D='.$v,'',$_SERVER['REQUEST_URI'])?>" class="icon-cancel"></a></li>  
      <?}?>
    </ul>

    <div class="portfolio_list flip_list clearfix">
<div class="row clearfix">
    <?
$n=0;
foreach ($arResult['PROJECTS'] as $k => $v) {

  if($n==4){
    $n=0;
    ?>
  </div>
   <div class="row clearfix">
  <?}
  ?>
  <div class="item" style="background: <?=$v['PROPERTY_M_BG_VALUE']?>;">
          <a href="<?=$v['DETAIL_PAGE_URL']?>" class="link"></a>
          <h3 class="title"><?=$v['~NAME']?></h3>
          <p><?=$v['PREVIEW_TEXT']?></p>
          <span><?foreach ($v['PROPERTY_NAPRAV_VALUE'] as $k2 => $v2){?><?=($k2>0)?', ':'';?><?=$arResult['NAPRAV'][$v2]?><?}?></span>
          <div class="picture"><img src="<?=($v['PREVIEW_PICTURE']['SRC']!='')?$v['PREVIEW_PICTURE']['SRC']:'/images/noImg_240x180.jpg';?>" alt=""></div>
        </div>
  
<?
  $n++;
}?>
  
      </div>
    </div>

    <div class="bottom_block"></div>
  </div>

