<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'CACHE_MEW_OBR';
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
	
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_CATEGORIES_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE"));
    while($arFields = $res->GetNext()) {
        $arResult['CATEGORIES'][$arFields['ID']]= $arFields;
    }
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_CLIENTS_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE"));
    while($arFields = $res->GetNext()) {
    	
        $arResult['CLIENTS'][$arFields['ID']]= $arFields;
    }

    $arrFilter=array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y");
    if(($_REQUEST['type']=="projects"||$_REQUEST['type']=='')&&intval($_REQUEST['id'])>0&&is_array($arResult['CATEGORIES'][intval($_REQUEST['id'])])){
    	$arrFilter['PROPERTY_CATEGORY']=$_REQUEST['id'];
    }elseif($_REQUEST['type']=="clients"&&intval($_REQUEST['id'])>0&&is_array($arResult['CLIENTS'][intval($_REQUEST['id'])])){
    	$arrFilter['PROPERTY_CLIENT']=$_REQUEST['id'];
    }
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), $arrFilter, false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE"));
    while($arFields = $res->GetNext()) {
    	$arButtons = CIBlock::GetPanelButtons(
			$arFields["IBLOCK_ID"],
			$arFields["ID"],
			0,
			array("SECTION_BUTTONS"=>true, "SESSID"=>false, "CATALOG"=>true)
		);
		$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
		$arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
		
        $arResult['PROJECTS'][$arFields['ID']]= $arFields;
    }
	
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}
$this->__component->arResult = $arResult; 
?>