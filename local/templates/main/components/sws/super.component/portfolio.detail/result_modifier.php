<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'DETAIL'.$arParams['CODE'];
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
    
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_NAPRAV_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SROK","PROPERTY_MAIN","PROPERTY_NAPRAV"));
    while($arFields = $res->GetNext()) {
    	$arResult['NAPRAV'][$arFields['ID']]=$arFields['NAME'];
    }
     $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_OTRASLI_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE"));
    while($arFields = $res->GetNext()) {
      $arResult['OTRASLI'][$arFields['ID']]=$arFields['NAME'];
    }
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'],'CODE'=>$_REQUEST['CODE'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SROK","PROPERTY_MAIN","PROPERTY_NAPRAV","PROPERTY_TYPE","DETAIL_TEXT","PROPERTY_SCREENS","PROPERTY_M_BG","PROPERTY_R_BG","PROPERTY_M_COLOR","PROPERTY_H_COLOR","PROPERTY_S_BG","PROPERTY_MSCREENS","PROPERTY_LINKS","DETAIL_TEXT","PROPERTY_PROJECTS",'PROPERTY_OTRASLI','PROPERTY_AD_CSS','PROPERTY_HTML',"PROPERTY_HEAD_TEXT"));
    if($arFields = $res->GetNext()) {
      //p($arFields);
        foreach ($arFields['PROPERTY_MSCREENS_VALUE'] as $k => $v) {
        	$arFields['PROPERTY_MSCREENS_VALUE'][$k]=CFile::GetFileArray($v);
        }
        foreach ($arFields['PROPERTY_SCREENS_VALUE'] as $k => $v) {
        	$arFields['PROPERTY_SCREENS_VALUE'][$k]=CFile::GetFileArray($v);
        }
        $arFields['PROPERTY_S_BG_VALUE']=CFile::GetFileArray($arFields['PROPERTY_S_BG_VALUE']);
      	
        $arResult['PROJECT']=$arFields;
        $arResult['PREV'] = $arResult['CURRENT'] = $arResult['NEXT'] = array();  

        //предыдущая и следующая 
        if (\Bitrix\Main\Loader::includeModule('iblock')) {
           $i = 0;
           $res = CIBlockElement::getList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arParams['IBLOCK_ID'],"PROPERTY_NO_SHOW"=>false),
                                   false, array('nElementID' => $arResult['PROJECT']['ID'], 'nPageSize' => 1),
                                   array('ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL'));
           while ($row = $res->getNext()) {
              $row['PREVIEW_PICTURE'] = CFile::GetFileArray($row['PREVIEW_PICTURE']);
              if ($arResult['PROJECT']['ID'] == $row['ID']) {
                 $arResult['CURRENT'] = $row;
              } elseif ($i > 0 && !empty($arResult['CURRENT'])) {
                 $arResult['NEXT'] = $row;
              }
              if ($i == 0 && empty($arResult['CURRENT'])) {
                 $arResult['PREV'] = $row;
              }
              $i++;
           }
           if(empty($arResult['PREV'])){
	           $res = CIBlockElement::getList(array('SORT' => 'DESC', 'ID' => 'ASC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arParams['IBLOCK_ID'],"PROPERTY_NO_SHOW"=>false),
	                                   false, array('nTopCount' => 1),
	                                   array('ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL'));
	           while ($row = $res->getNext()) {
		             $row['PREVIEW_PICTURE'] = CFile::GetFileArray($row['PREVIEW_PICTURE']);
	                 $arResult['PREV'] = $row;
	           }
           }
           if(empty($arResult['NEXT'])){
	           $res = CIBlockElement::getList(array('SORT' => 'ASC', 'ID' => 'DESC'), array('ACTIVE' => 'Y', 'IBLOCK_ID' => $arParams['IBLOCK_ID'],"PROPERTY_NO_SHOW"=>false),
	                                   false, array('nTopCount' => 1),
	                                   array('ID', 'NAME', 'PREVIEW_PICTURE', 'DETAIL_PAGE_URL'));
	           while ($row = $res->getNext()) {
		             $row['PREVIEW_PICTURE'] = CFile::GetFileArray($row['PREVIEW_PICTURE']);
	                 $arResult['NEXT'] = $row;
	           }
           }
        }
        if(count($arFields['PROPERTY_PROJECTS_VALUE'])>0){
          $res2 = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'],'ID'=>$arFields['PROPERTY_PROJECTS_VALUE'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SROK","PROPERTY_NAPRAV"));
          while ($arFields2=$res2->GetNext()) {
             $arFields2['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields2['PREVIEW_PICTURE']); 
            $arResult['PROJECTS'][$arFields2['ID']]=$arFields2;
          }
        }
        $res2 = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_BLOCKS_ID'],'PROPERTY_PROJECT'=>$arFields['ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PROPERTY_RED",'PROPERTY_NUMBER'));
          while ($arFields2=$res2->GetNext()) {
            $arResult['BLOCKS'][]=$arFields2;
          }
    }else{

    }
    $css='';
    if($arResult['PROJECT']['PROPERTY_H_COLOR_VALUE']!=''){
      $css='.project_head .title {color: '.$arResult['PROJECT']['PROPERTY_H_COLOR_VALUE'].';}';
    }else{
      $css='.project_head .title {color: #cb1300;}';
    }
    if($arResult['PROJECT']['PROPERTY_M_BG_VALUE']!=''){
		if($arResult['PROJECT']['PROPERTY_R_BG_VALUE']!=''){
			$css.='body {background: linear-gradient(90deg, '.$arResult['PROJECT']['PROPERTY_M_BG_VALUE'].', '.$arResult['PROJECT']['PROPERTY_R_BG_VALUE'].');}';
		}else{
			$css.='body {background: '.$arResult['PROJECT']['PROPERTY_M_BG_VALUE'].';}';
		}
    }

    if($arResult['PROJECT']['PROPERTY_M_COLOR_VALUE']!=''){
      $css.='.relay,.relay.border{border-color:'.$arResult['PROJECT']['PROPERTY_M_COLOR_VALUE'].';}';
      $css.='.project_links a{color:'.$arResult['PROJECT']['PROPERTY_M_COLOR_VALUE'].';}';
      $css.='.header-wrap .header_text{color:'.$arResult['PROJECT']['PROPERTY_M_COLOR_VALUE'].';}';
      $css.='.project_menu li a{border-color:'.$arResult['PROJECT']['PROPERTY_M_COLOR_VALUE'].';}';
    }
    if($arResult['PROJECT']['PROPERTY_AD_CSS_VALUE']!=''){
      $css.=$arResult['PROJECT']['PROPERTY_AD_CSS_VALUE'];
    }
    
    if(!file_exists($_SERVER['DOCUMENT_ROOT']."/upload/css/")){
      mkdir($_SERVER['DOCUMENT_ROOT']."/upload/css/");
    }
    file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/css/".$arResult['PROJECT']['ID'].".css",$css);
    
    //p($arResult);
    /*d($arResult['ARTISTS']);*/
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	//file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->__folder."/style.css",$css);
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}

//$APPLICATION->SetAdditionalCSS();


$this->__component->arResult = $arResult; 
?>