<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="clearfix relay">
  <?if($arResult['PREV']['NAME']!=''){?>
    <div class="fl_l"><a href="<?=$arResult['PREV']['DETAIL_PAGE_URL']?>"><i class="fa fa-angle-left" aria-hidden="true"></i><?=$arResult['PREV']['~NAME']?></a></div>
  <?}?>
  <?if($arResult['NEXT']['NAME']!=''){?>
    <div class="fl_r"><a href="<?=$arResult['NEXT']['DETAIL_PAGE_URL']?>"><?=$arResult['NEXT']['~NAME']?><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
  <?}?>
  </div>
  <div class="project_head white_container clearfix">
  <?//p($arResult['PROJECT']);?>
    <h1 class="title"><?=$arResult["PROJECT"]['~NAME']?></h1>
	<?if($arResult['PROJECT']['~PROPERTY_HEAD_TEXT_VALUE']['TEXT']):?>
		<h3 class="second_title"><?=$arResult['PROJECT']['~PROPERTY_HEAD_TEXT_VALUE']['TEXT']?></h3>
	<?else:?>
		<h3 class="second_title"><?=$arResult["PROJECT"]['~PREVIEW_TEXT']?></h3>
	<?endif;?>
    <p class="description"></p>
    <span class="date"><?=$arResult["PROJECT"]['PROPERTY_SROK_VALUE']?></span>
    <ul class="project_menu">
     <?foreach ($arResult['PROJECT']['PROPERTY_NAPRAV_VALUE'] as $k => $v) {?>
       <li><a href="/portfolio/search/?search=&napravlenia%5B%5D=<?=$v?>"><?=$arResult['NAPRAV'][$v]?></a></li>
     <?}?>
     <?/*
	 foreach ($arResult['PROJECT']['PROPERTY_OTRASLI_VALUE'] as $k => $v) {?>
       <li><a href="/portfolio/search/?search=&otrasl%5B%5D=<?=$v?>"><?=$arResult['OTRASLI'][$v]?></a></li>
     <?}
	 */?>
    </ul>
  </div>
</section>
<?
if($arResult['PROJECT']['PROPERTY_TYPE_ENUM_ID']==3){

if(count($arResult['PROJECT']['PROPERTY_MSCREENS_VALUE'])>0){?>
<section>
  <div class="project_gallery">
    <div class="center_container"><div class="background" style="background-image: url('<?=$arResult['PROJECT']['PROPERTY_S_BG_VALUE']['SRC']?>');"></div></div>
    <!-- Swiper -->
      <div class="swiper-container">
          <div class="swiper-wrapper">
            <?foreach ($arResult['PROJECT']['PROPERTY_MSCREENS_VALUE'] as $k => $v) {?>
              <div class="swiper-slide">
                <img src="<?=$v['SRC']?>" alt="">
              </div>
            <?}?>


          </div>
          <!-- Add Pagination -->
          <div class="swiper-pagination"></div>
      <!-- Add Navigation -->
      <div class="swiper-button-prev swiper-button-white"></div>
      <div class="swiper-button-next swiper-button-white"></div>
      </div>

      <!-- Initialize Swiper -->
      <script>
      var swiper = new Swiper('.swiper-container', {
          pagination: '.swiper-pagination',
          centeredSlides: true,
          paginationClickable: true,
          spaceBetween: 0,
          speed: 900,
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev',
          slidesPerView: 'auto',
          loop: true

      });
      </script>
  </div>
</section>
<?}?>

<section class="main bottom_row">
<?
if(count($arResult['BLOCKS'])>0){?>
  <div class="project_item_list">
    <ul class="col-3 clearfix">

    <?
    $n=1;
    $first=true;
    foreach ($arResult['BLOCKS'] as $k => $v) {
        $n++;?>

      <li>
        <?if($v['PROPERTY_RED_VALUE']!=''){?>
          <span class="top"></span>
        <?}?>
        <span class="number"><?=$v['PROPERTY_NUMBER_VALUE']?></span>
        <strong><?=$v['~NAME']?></strong>
        <p><?=$v['~PREVIEW_TEXT']?></p>
      </li>
       <?

       if($n % 4 == 0&&$first) {
        $n=1;

        $first=false;
        ?></ul>
    <ul class="col-2 clearfix"><?
      }
      if($n % 3 == 0&&!$first) {
        $n=1;
        $first=true;

      ?>
      </ul>
    <ul class="col-3 clearfix">
      <?}

    }?>


    </ul>

  </div>
  <?}?>
<div class="project_details">
    <div class="text">
      <h3 class="title">детали проекта</h3>
      <?=$arResult['PROJECT']['~DETAIL_TEXT']?>
    </div>
    <div class="gallery clearfix">
      <ul class="masonry">
      <?foreach ($arResult['PROJECT']['PROPERTY_SCREENS_VALUE'] as $k => $v) {?>
        <li><img src="<?=$v['SRC']?>" alt=""></li>
      <?}?>

      </ul>
    </div>
    <script>
    $(window).load(function(){ $('.masonry').masonry({
      fitWidth: true
    }); });
    </script>
  </div>
  <div class="project_links">
  <?foreach ($arResult['PROJECT']['PROPERTY_LINKS_VALUE'] as $k => $v) {?>
    <a target="_blank" href="<?=$v?>" class="<?=($arResult['PROJECT']['PROPERTY_LINKS_DESCRIPTION'][$k]=='')?'site':$arResult['PROJECT']['PROPERTY_LINKS_DESCRIPTION'][$k];?>"><?=$v?></a>
  <?}?>


  </div>
  <?//p($arResult);?>
  <div class="clearfix relay border">
    <div class="fl_l"><a href="<?=$arResult['PREV']['DETAIL_PAGE_URL']?>"><i class="fa fa-angle-left" aria-hidden="true"></i><?=$arResult['PREV']['~NAME']?></a></div>
    <div class="fl_r"><a href="<?=$arResult['NEXT']['DETAIL_PAGE_URL']?>"><?=$arResult['NEXT']['~NAME']?><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
  </div>

 <?if(count($arResult['PROJECT']['PROPERTY_PROJECTS_VALUE'])>0){
  ?> <div class="interest_header">
    <h3 class="second_title">Вам также будет интересно</h3>
  </div>

<?}?>
</section>
<?}else{?>
<?=$arResult['PROJECT']['~PROPERTY_HTML_VALUE']['TEXT'];?>
<section  class="main bottom_row">
  <div class="clearfix relay border">
    <div class="fl_l"><a href="<?=$arResult['PREV']['DETAIL_PAGE_URL']?>"><i class="fa fa-angle-left" aria-hidden="true"></i><?=$arResult['PREV']['~NAME']?></a></div>
    <div class="fl_r"><a href="<?=$arResult['NEXT']['DETAIL_PAGE_URL']?>"><?=$arResult['NEXT']['~NAME']?><i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
  </div>

 <?if(count($arResult['PROJECT']['PROPERTY_PROJECTS_VALUE'])>0){
  ?> <div class="interest_header">
    <h3 class="second_title">Вам также будет интересно</h3>
  </div>

<?}?>
</section>

<?}?>
<?if(count($arResult['PROJECT']['PROPERTY_PROJECTS_VALUE'])>0){
  ?>
<section>
  <div class="interest col3 clearfix">
 <?foreach ($arResult['PROJECTS'] as $k => $v) {?>
  <div class="item" style="background-image:url('<?=$v['PREVIEW_PICTURE']['SRC']?>');">
    <a href="<?=$v['DETAIL_PAGE_URL']?>" class="title-link"></a>
          <div class="title"><?=$v['NAME']?></div>
          <div class="bottom">
            <p><?=$v['PREVIEW_TEXT']?></p>
            <ul>
              <?foreach ($v['PROPERTY_NAPRAV_VALUE'] as $k2 => $v2) {?>
                <li><?=$arResult['NAPRAV'][$v2]?></li>
              <?}?>
            </ul>
            <p><small><?=$v['PROPERTY_SROK_VALUE']?></small></p>
          </div>
        </div>
<?}?>
  </div>
</section>
<?}?>
<section class="main bottom_row">
  <div class="bottom_block"></div>
</section>