<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$APPLICATION->SetTitle('Проекты - '.$arResult['PROJECT']['~NAME']);
$APPLICATION->AddHeadString('<link href="/upload/css/'.$arResult['PROJECT']['ID'].'.css"  type="text/css" rel="stylesheet" />',true);
$APPLICATION->AddChainItem($arResult['PROJECT']['NAME'], $arResult['PROJECT']['DETAIL_PAGE_URL']);
$APPLICATION->SetAdditionalCSS("/js/swiper/swiper.min.css");
$APPLICATION->AddHeadScript('/js/swiper/swiper.min.js');
$APPLICATION->AddHeadScript('/js/main_slider.js');
?>