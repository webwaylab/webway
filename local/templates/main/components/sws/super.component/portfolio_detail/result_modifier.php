<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'CACHE_MEW_OBR'.$arParams['CODE'];
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
	
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_CATEGORIES_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE"));
    while($arFields = $res->GetNext()) {
        $arResult['CATEGORIES'][$arFields['ID']]= $arFields;
    }
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_CLIENTS_ID'], "ACTIVE" => "Y"), false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE"));
    while($arFields = $res->GetNext()) {
    	
        $arResult['CLIENTS'][$arFields['ID']]= $arFields;
    }
   /* echo $arParams['CODE'];*/
    $arrFilter=array(
    	"IBLOCK_ID" => $arParams['IBLOCK_ID'], 
    	"ACTIVE" => "Y",
    	"CODE"=>$arParams['CODE']
    	);
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), $arrFilter, false, false, array("ID", "NAME", "PREVIEW_TEXT","PREVIEW_PICTURE","PROPERTY_SNOSKA","PROPERTY_LINK",'PROPERTY_PICTURES','DETAIL_TEXT','DETAIL_PICTURE'));
    if($arFields = $res->GetNext()) {
    	$arButtons = CIBlock::GetPanelButtons(
			$arFields["IBLOCK_ID"],
			$arFields["ID"],
			0,
			array("SECTION_BUTTONS"=>true, "SESSID"=>false, "CATALOG"=>true)
		);
		$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
		$arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
		$arFields['DETAIL_PICTURE'] = CFile::GetFileArray($arFields['DETAIL_PICTURE']);
		foreach ($arFields['PROPERTY_SNOSKA_VALUE'] as $k => $v) {
			$arFields['PROPERTY_SNOSKA_VALUE'][$k] = CFile::GetFileArray($v);
		}
		foreach ($arFields['PROPERTY_PICTURES_VALUE'] as $k => $v) {
			$arFields['PROPERTY_PICTURES_VALUE'][$k] = CFile::GetFileArray($v);
		}
		
        $arResult['PROJECT']=$arFields;
    }
	$APPLICATION->SetTitle($arResult['PROJECT']['NAME']." - Портфолио - Simple Web Solutions");
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}
$this->__component->arResult = $arResult; 
?>