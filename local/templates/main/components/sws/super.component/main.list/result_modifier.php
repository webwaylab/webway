<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'LIST'.$arParams['CODE'];
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
    $arSlides=[];
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID_SLIDES'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE","PROPERTY_COLOR","PROPERTY_TEXT_COLOR"));
    while($arFields = $res->GetNext()) {
        $arSlides[]=$arFields['ID'];
        $arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
        $arResult['SLIDES'][]=$arFields;
    }
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID_ELEMENTS'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","PROPERTY_SLIDE"=>$arSlides), false, false, array("ID", "NAME","PREVIEW_TEXT","PREVIEW_PICTURE","PROPERTY_LINK","PROPERTY_SLIDE"));
    while($arFields = $res->GetNext()) {
        $arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
        $arResult['SLIDES_EK'][$arFields['PROPERTY_SLIDE_VALUE']][]=$arFields;
    }
   // p($arResult);
     $css='';
      foreach ($arResult['SLIDES'] as $k => $v) {
        if($k!=count($arResult['SLIDES'])-1){
          $next_color = $arResult['SLIDES'][$k+1]['PROPERTY_COLOR_VALUE'];
        }else{
          $next_color = $arResult['SLIDES'][0]['PROPERTY_COLOR_VALUE'];
        }
        if($v['PROPERTY_TEXT_COLOR_VALUE']!=''){
          $text_color=$v['PROPERTY_TEXT_COLOR_VALUE'];
        }else{
          $text_color=$v['PROPERTY_COLOR_VALUE'];
        }
        $css.=".swiper-container.gallery-thumbs .item".($k+1)." {\n";
        $css.="background: #".$v['PROPERTY_COLOR_VALUE'].";\n";
        $css.="background: -webkit-linear-gradient(legacy-direction(90deg), ".$v['PROPERTY_COLOR_VALUE'].", #".$next_color.");\n";
        $css.="background: linear-gradient(90deg, #".$v['PROPERTY_COLOR_VALUE'].", #".$next_color.");\n";
        $css.="}\n";
        $css.="body.item".($k+1)." .active-menu .header_menu_button, .main_swiper .swiper-button-next {\n";  
        $css.="background: #".$v['PROPERTY_COLOR_VALUE'].";\n";
        $css.="background: -webkit-linear-gradient(legacy-direction(90deg), ".$v['PROPERTY_COLOR_VALUE'].", #".$next_color.");\n";
        $css.="background: linear-gradient(90deg, #".$v['PROPERTY_COLOR_VALUE'].", #".$next_color.");\n";
        $css.="}\n";
        $css.="body.item".($k+1)." .header_menu_overflow ul li a span {\n";
        $css.="color: #".$text_color.";\n";
        $css.="}\n";
        $css.="body.item".($k+1)." .header_text {\n";
        $css.="color: #".$text_color.";\n";
        $css.="}\n";  
        $css.="body.item".($k+1)." .main_swiper .swiper-container.gallery-top .swiper-slide.swiper-slide-active h3.title, body.item".($k+1)." .main_swiper .swiper-container.gallery-top .swiper-slide.swiper-slide-active .title_mobile  {\n";
        $css.="color: #".$text_color.";\n";
        $css.="}\n";
		$css.="body.item".($k+1)." .main_swiper .swiper-button-next-backgraund {\n";  
		$css.="background: #".$v['PROPERTY_COLOR_VALUE'].";\n";
		$css.="background: -webkit-linear-gradient(legacy-direction(90deg), ".$v['PROPERTY_COLOR_VALUE'].", #".$next_color.");\n";
		$css.="background: linear-gradient(90deg, #".$v['PROPERTY_COLOR_VALUE'].", #".$next_color.");\n";
		$css.="width: 54px; height: 54px; position: absolute; right: 130px; top: 179px;z-index: 90;\n";
		$css.="}\n";
      }
  	$css.="body .main_swiper .swiper-container.gallery-top .swiper-button-white{right: 0;top: 22px;}";

    /*d($arResult['ARTISTS']);*/
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->__folder."/style.css",$css);
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}

$this->__component->arResult = $arResult; 
?>