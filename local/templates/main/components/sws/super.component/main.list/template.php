<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?


  ?>

 <div class="main_swiper">
    <!-- Swiper -->

  <div class="swiper-container gallery-thumbs">
      <div class="swiper-wrapper">
         <?foreach ($arResult['SLIDES'] as $k => $v) {?>
           <div class="swiper-slide item<?=$k+1?>"></div>
         <?}?>
      </div>
  </div>
  <div class="swiper-container gallery-top">
      <div class="swiper-wrapper">
        <?foreach ($arResult['SLIDES'] as $k => $v) {?>
          <div class="swiper-slide">
          <h3 class="title" data-swiper-parallax="-100"><?=$v['NAME']?></h3>
          <div class="airship" style="background-image:url('<?=($v['PREVIEW_PICTURE']['SRC']!='')?$v['PREVIEW_PICTURE']['SRC']:'/images/airship1.png';?>');" data-swiper-parallax="-1200"></div>
          <div class="description" data-swiper-parallax="-2500">
            <span class="title_mobile"><?=$v['NAME']?></span>
            <p><?=$v['~PREVIEW_TEXT']?></p>
          </div>
          <ul class="case_list clearfix">
            <?foreach ($arResult['SLIDES_EK'][$v['ID']] as $k2 => $v2) {?>
              <li>
                  <a href="<?=($v2['PROPERTY_LINK_VALUE']!='')?$v2['PROPERTY_LINK_VALUE']:'#';?>">
                    <img src="<?=($v2['PREVIEW_PICTURE']['SRC'])?$v2['PREVIEW_PICTURE']['SRC']:'/images/noImg.jpg';?>" alt="" />
                    <span class="title"><?=$v2['~PREVIEW_TEXT']?></span>
                  </a>
              </li>
            <?}?>
<!--             <li>
              <div class="last">
                <img src="/images/main-project.png" alt="" />
                <span class="title">займи свое место</span>
              </div>
            </li> --> 
          </ul>
        </div>
        <?}?>
      </div>

      <!-- Add Pagination -->
      <div class="swiper-pagination"></div>

  <!-- <div class="swiper-button-prev swiper-button-white"></div> -->
      <div class="swiper-button-next-backgraund">
      	<div class="swiper-button-next swiper-button-white"></div>
      </div>
    </div>

  </div>
</div>
  <style type="text/css">
  <?
  echo $css;
?>
</style>