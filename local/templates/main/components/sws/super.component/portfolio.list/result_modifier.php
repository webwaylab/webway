<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$obCache_trade = new CPHPCache();
$CACHE_ID = 'LIST'.$arParams['CODE'];
if ( $obCache_trade->InitCache($arParams['CACHE_TIME'], $CACHE_ID, '/') )
{
	$arResult = $obCache_trade->GetVars();
}
else
{
	
	$obCache_trade->StartDataCache();
	CModule::IncludeModule('iblock');
    
	$arResult = array();
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_NAPRAV_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE"));
    while($arFields = $res->GetNext()) {
    	$arResult['NAPRAV'][$arFields['ID']]=$arFields['NAME'];
    }
    $res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_OTRASLI_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y"), false, false, array("ID", "NAME","CODE"));
    while($arFields = $res->GetNext()) {
    	$arResult['OTRASLI'][$arFields['ID']]=$arFields['NAME'];
    }
	$res = CIBlockElement::GetList(array("SORT" => "ASC"), array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y","ACTIVE_DATE"=>"Y","PROPERTY_MAIN"=>"1"), false, false, array("ID", "NAME","CODE", "PREVIEW_TEXT","PREVIEW_PICTURE","DETAIL_PAGE_URL","PROPERTY_SROK","PROPERTY_MAIN","PROPERTY_NAPRAV"));
    while($arFields = $res->GetNext()) {
    	$arButtons = CIBlock::GetPanelButtons(
			$arFields["IBLOCK_ID"],
			$arFields["ID"],
			0,
			array("SECTION_BUTTONS"=>true, "SESSID"=>false, "CATALOG"=>true)
		);
		$arFields["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arFields["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];
        $arFields['PREVIEW_PICTURE'] = CFile::GetFileArray($arFields['PREVIEW_PICTURE']);
        $arResult['PROJECTS'][]=$arFields;
    }

    //p($arResult['PROJECTS']);
    /*d($arResult['ARTISTS']);*/
	// saving template name to cache array
	$arResult["__TEMPLATE_FOLDER"] = $this->__folder;
	//file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->__folder."/style.css",$css);
	// writing new $arResult to cache file
	$obCache_trade->EndDataCache($arResult);

}

$this->__component->arResult = $arResult; 
?>