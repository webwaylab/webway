<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="white_container clearfix">
<div class="portfolio_nav">
      <a href="/portfolio/" class="active">Лучшие проекты</a>
      <a href="/portfolio/search/">Проекты по направлениям</a>
    </div>

    <div class="interest portfolio flip_list clearfix">
    <div class="clearfix">
<?
$n=1;
foreach ($arResult['PROJECTS'] as $k => $v) {
  $n++;
  if($n==2){
    $n=0;
    ?>
  </div>
   <div class="clearfix">
  <?}
  ?>
  <div class="item" style="background-image:url('<?=$v['PREVIEW_PICTURE']['SRC']?>');">
          <a href="<?=$v['DETAIL_PAGE_URL']?>" class="link"></a>
          <h3 class="title"><?=$v['NAME']?></h3>
          <div class="bottom">
            <p><?=$v['PREVIEW_TEXT']?></p>
            <ul>
              <?foreach ($v['PROPERTY_NAPRAV_VALUE'] as $k2 => $v2) {?>
                <li><?=$arResult['NAPRAV'][$v2]?></li>
              <?}?>
            </ul>
            <p><small><?=$v['PROPERTY_SROK_VALUE']?></small></p>
          </div>
        </div>
<?}?>
  </div>
    <div class="bottom_block">
      <div class="project_links">
        <a href="/portfolio/search/">Еще</a>
      </div>
    </div>
  </div>