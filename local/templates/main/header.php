<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>


<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" >
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<?$APPLICATION->ShowHead();?>
	<title><?$APPLICATION->ShowTitle()?></title>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-4577305-2']);
        _gaq.push(['_addOrganic', 'go.mail.ru', 'q']);
        _gaq.push(['_addOrganic', 'nova.rambler.ru', 'query', true]);
        _gaq.push(['_addOrganic', 'search.qip.ru', 'query']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</head>
<?
$APPLICATION->SetAdditionalCSS("/css/reset.css");
$APPLICATION->SetAdditionalCSS("/css/animate.css");
$APPLICATION->SetAdditionalCSS("/css/font-awesome.css");
$APPLICATION->SetAdditionalCSS("/css/style.css");
$APPLICATION->SetAdditionalCSS("/js/multiselect/jquery.multiselect.css");
$APPLICATION->AddHeadScript('http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');
$APPLICATION->AddHeadScript('http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js');
$APPLICATION->AddHeadScript('/js/multiselect/jquery.multiselect.js');
$APPLICATION->AddHeadScript('/js/masonry.pkgd.min.js');
$APPLICATION->AddHeadScript('/js/script.js');
?>
<body>
<?$APPLICATION->ShowPanel()?>
<div class="header-wrap">
	<header class="header clearfix">

		<div class="left">
			<a href="#header_menu" class="menu-link header_menu_button popup-with-move-anim" id="menu-opener" alt="menu"><span>Разделы</span> <i class="icon-menu"></i></a>      

			<div class="logo"><a href="/" alt="webway">webway</a></div>

			<span class="header_text"><?=BIRTHDAY_YEARS?> в пути</span>
		</div>
		<div class="right">
			<form action="/portfolio/search/" class="header_search">
				<input name="search" type="text" placeholder="Поиск" />
				<input type="submit" class="icon-search" value="искать" />
			</form>
			<a href="<?=$APPLICATION->GetProperty("facebook");?>" class="header_social fb" title="" target="_blank"><i class="icon-facebook-squared"></i></a>
			<a href="/contacts/" class="header_location"><i class="icon-location"></i></a>
		</div>          
	</header>  <!-- //END HEADER BLOCK -->
</div>

 <?$APPLICATION->IncludeComponent("bitrix:menu", "top", array(
				"ROOT_MENU_TYPE" => "top",
				"MAX_LEVEL" => "2",
				"CHILD_MENU_TYPE" => "left",
				"USE_EXT" => "Y",
				"MENU_CACHE_TYPE" => "A",
				"MENU_CACHE_TIME" => "36000000",
				"MENU_CACHE_USE_GROUPS" => "Y",
				"MENU_CACHE_GET_VARS" => ""
				),
				false,
				array(
				"ACTIVE_COMPONENT" => "Y"
				)
			);?>

  <section class="main">
<?if($APPLICATION->GetCurPage()=="/"){?>
	
<?}else{?>
	
	<?$APPLICATION->IncludeComponent("bitrix:breadcrumb", "main", Array(
		"PATH" => "",	
			"SITE_ID" => "s1",	
			"START_FROM" => "0",
		),
		false
	);?>
<?}?>